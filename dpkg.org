; ds-in-parent = yes

$TTL 8h

@                       IN      NS      sec1.rcode0.net.
@                       IN      NS      sec2.rcode0.net.
@                       IN      NS      nsp.dnsnode.net.
@                       IN      NS      dns4.easydns.info.
@                       IN      TXT     "dpkg.org, package maintenance system for Debian"
_whois                  IN      CNAME   _whois.debian.org.

$TTL 30m

@                       IN      MX      10 mail.debconf.org.
@                       IN      MX      20 smithers.debconf.org.

$TTL 8h

www                     IN      CNAME   hadrons.org.
git                     IN      CNAME   hadrons.org.

; vim: syn=dns:
